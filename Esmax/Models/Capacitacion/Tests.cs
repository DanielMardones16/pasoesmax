using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.Capacitacion
{
    public class Tests
    {
        [Key] 
        public int id_test { get; set; }

        public string nombre { get; set; }

        public string estado { get; set; }

        public virtual ICollection<PregTests> PregTests { get; set; }

        public virtual ICollection<ResultadosTest> ResultadosTests { get; set; }

    }
}