using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.Capacitacion
{
    public class PregTests
    {
        public int id_test { get; set; }

        public string descripcion { get; set; }
        
        [Key]
        public string id_pregunta { get; set; }

        public string a { get; set; }
        
        public string b { get; set; }
        
        public string c { get; set; }
        
        public string d { get; set; }
        
        public string e { get; set; }
        
        public string correcta { get; set; }
    }
}