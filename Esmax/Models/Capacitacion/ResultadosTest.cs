using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.Capacitacion
{
    public class ResultadosTest
    {
        [Key]
        public int id_resultado { get; set; }

        public string id_user { get; set; }

        public int id_test { get; set; }

        public string resp1 { get; set; }
        
        public string resp2 { get; set; }

        public string resp3 { get; set; }

        public string resp4 { get; set; }

        public string resp5 { get; set; }

        public string resp6 { get; set; }

        public string resp7 { get; set; }

        public string resp8 { get; set; }

        public string resp9 { get; set; }

        public string resp10 { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? fecha_test_tomado { get; set; }

        public bool aprobado { get; set; }





    }
}