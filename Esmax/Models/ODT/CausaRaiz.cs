using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models
{
    public class CausaRaiz
    {
        [Key]
        public int causa_raiz_id { get; set; }
        public string descripcion { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; } 
        public virtual ICollection<PlanAccion> PlanAccion { get; set; }
    }
 }