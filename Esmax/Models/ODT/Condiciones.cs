using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models
{
    public class Condiciones
    {
        [Key]
        public int condiciones_id { get; set; }
        public int OdtSubTareas_id { get; set; }  
        public int valor { get; set; }
        public int respuesta_odt_id { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; } 
        
        public virtual RespuestaOdt RespuestaOdt { get; set; }
        public virtual ICollection<OdtSubTareas> OdtSubTareas { get; set; }
        
    }
}