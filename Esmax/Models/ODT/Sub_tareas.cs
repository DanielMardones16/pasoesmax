﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Esmax.Models
{
    public class Sub_tareas
    {
        [Key]
        public int sub_tareas_id { get; set; }
        public string sub_tareas_nombre { get; set; }
        public int tareas_id { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? created_at { get; set; }

        public virtual Tareas Tareas { get; set; }
        public virtual ICollection<OdtSubTareas>  OdtSubTareas { get; set; }
    }
}