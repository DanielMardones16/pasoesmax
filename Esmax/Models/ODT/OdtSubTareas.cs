using System.ComponentModel.DataAnnotations;

namespace Esmax.Models
{
    public class OdtSubTareas
    {
        [Key]
        public int OdtSubTareas_id { get; set; }
        public int odts_id { get; set; }
        public int sub_tareas_id { get; set; }

        public virtual Odts Odts { get; set; }
        public virtual Sub_tareas Sub_tareas { get; set; }
        public virtual Observaciones Observaciones { get; set; }

    }
}