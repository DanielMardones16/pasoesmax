﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.UI.WebControls;

namespace Esmax.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Correo electrónico")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Código")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "¿Recordar este explorador?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Correo electrónico")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        public string Chave { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [Display(Name = "¿Recordar cuenta?")]
        public bool RememberMe { get; set; }
    }

    
    
    
    public class RegisterViewModel
    {
        [Required]
        [StringLength(10, MinimumLength = 8)]
        [Display(Name = "ID")]
        public string Id { get; set; }
        
        [Required]
        [Display(Name = "Usuario")]
        public string Usuario { get; set; }
        
        [Required]
        [StringLength(10, MinimumLength = 9)]
        [RegularExpression("^(\\d{1}|\\d{2})(\\d{3}\\d{3}-)([a-zA-Z]{1}$|\\d{1}$)", ErrorMessage = "El formato del Rut debe ser 99999999-9")]
        public string Rut { get; set; }
        
        
        [Required]
        [RegularExpression ("^[a-zA-Z- ]*$" , ErrorMessage = "El Cargo solo puede contener letras")]
        [Display(Name = "Cargo")]
        public string Cargo { get; set; }
        
        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Fecha de ingreso")]
        public DateTime Fecha_Ingreso { get; set; }
        
        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Fecha de nacimiento")]
        public DateTime Fecha_Nacimiento { get; set; }
        
        [Required]
        [EmailAddress]
        [Display(Name = "Correo electrónico")]
        public string Email { get; set; }
        
        [Required]
        [StringLength(100, ErrorMessage = "El número de caracteres de {0} debe ser al menos {2}.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar contraseña")]
        [Compare("Password", ErrorMessage = "La contraseña y la contraseña de confirmación no coinciden.")]
        public string ConfirmPassword { get; set; }
        
        [Required]
        [RegularExpression ("^[a-zA-Z- ]*$" , ErrorMessage = "El nombre solo puede contener letras")]
        [Display(Name = "Nombre completo")]
        public string UserName { get; set; }   
        
        [Required]
        [Display(Name = "Rol")]
        public string Rol { get; set; }
        
        [Required]
        [Display(Name = "Chave")]
        public string Chave { get; set; }
        
        [Required]
        [Display(Name = "Telefono corporativo")]
        public int Telefono_corporativo { get; set; }
        
        [Required]
        [Display(Name = "Holding")]
        public int holding_id { get; set; }
        
        [Required]
        [Display(Name = "Negocio")]
        public int negocio_id { get; set; }
        
        [Required]
        [Display(Name = "Gerencia")]
        public int gerencia_id { get; set; }
        
        [Required]
        [Display(Name = "Area")]
        public int area_id { get; set; }
        
        
        
//        [Required]
//        [Display(Name = "Foto de perfil")]
    }

    
    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Correo electrónico")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "El número de caracteres de {0} debe ser al menos {2}.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar contraseña")]
        [Compare("Password", ErrorMessage = "La contraseña y la contraseña de confirmación no coinciden.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Correo electrónico")]
        public string Email { get; set; }
    }
}
