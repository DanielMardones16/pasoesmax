using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.CheckList
{
    public class DatosGeneralesCheckList
    {
        [Key]
        public int datosGeneralesCheckListId { get; set; }
        public int Observador_id { get; set; }
        public DateTime fecha { get; set; }
        public string hora { get; set; }
        public int holding_id { get; set; }
        public int negocio_id { get; set; }
        public int gerencia_id { get; set; }
        public int area_id { get; set; }
        public string observador_nombre { get; set; }
        public int anonimo { get; set; }
        public DateTime? created_at { get; set; }
        
        public virtual ICollection<RespuestaCheckList> RespuestaCheckLists { get; set; }
    }
}