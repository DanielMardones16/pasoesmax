using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.CheckList
{
    public class CheckList
    {
        [Key]
        public int CheckListId { get; set; }
        public string CheckListNombre { get; set; }
        public string CheckListDescripcion { get; set; }
        public int PlanAccion { get; set; }
        public int categorias_id { get; set; }

        public string estado { get; set; } = "habilitado";
        public DateTime? created_at { get; set; }
        
        
        //public virtual CategoriasC CategoriasC { get; set; }
        public virtual ICollection<Actividades> Actividades { get; set; }
        
    }
}