using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.CheckList
{
    public class Preguntas
    {
        [Key]
        public int PreguntaId { get; set; }
        public string pregunta { get; set; }
        public int ActividadId { get; set; }
        public DateTime? created_at { get; set; }
        public int Procentaje { get; set; }
        
        
        
        public virtual Actividades Actividades { get; set; }
        
    }
}