using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.CheckList
{
    public class CategoriasC
    {
        [Key]
        public int categorias_id { get; set; }
        public string categorias_nombre { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; }
        
        public virtual ICollection<CheckList> CheckList { get; set; }
    }
}