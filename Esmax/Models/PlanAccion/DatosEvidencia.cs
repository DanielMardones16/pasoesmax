using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.MplanAccion
{
    public class DatosEvidencia
    {
        [Key]
        public int datosEvidenciaId { get; set; }
        public string Descripcion { get; set; }
        public int? tareas_id { get; set; }
        public int? plan_accion_id { get; set; }
        public DateTime? created_at { get; set; }
        
        public virtual TareasPlanAccion TareasPlanAccion { get; set; }
        public virtual PlanAccion PlanAccion { get; set; }
        public virtual ICollection<EvidenciaTarea> EvidenciaTarea { get; set; }
        
    }
}