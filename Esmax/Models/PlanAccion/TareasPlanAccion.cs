using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.MplanAccion
{
    public class TareasPlanAccion
    {
        [Key]
        public int tareas_id { get; set; }
        public string tareas_descripcion { get; set; }
        public int plan_accion_id { get; set; }
        
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; }

        public string estado { get; set; } = "habilitado";
        
        public virtual PlanAccion PlanAccion { get; set; }
        public virtual ICollection<EvidenciaTarea> EvidenciaTareas { get; set; }
    }
}