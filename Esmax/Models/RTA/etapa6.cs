using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.RTA
{
    public class etapa6
    {
        [Key]
        public int etapa6_id { get; set; }

        public string eficacia_rta { get; set; }
        public int plazo_cumplido { get; set; }
        public string comentarios { get; set; }
        
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; }
        public virtual ICollection<RespuestaRTA> RespuestaRTA { get; set; }
    }
}