using System;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.RTA
{
    public class Acciones_corr_prev
    {
        [Key] public int id_accion { get; set; }
        public string tipo_accion { get; set; }
        public string accion { get; set; }
        public int responsable_id { get; set; }
        public string responsable_nombre { get; set; }
        public string tipo_evidencia { get; set; }
        public int etapa3_id { get; set; }
        public string estado { get; set; }
        public string justificacion { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? fecha_cumplimiento { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; }
        
        public etapa3 etapa3 { get; set; }
    }
}