using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Esmax.Models.RSMS;

namespace Esmax.Models.RTA
{
    public class Instalaciones
    {
        [Key]
        public int instalaciones_id { get; set; }
        public string instalaciones_nombre { get; set; } 
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; }
        public virtual ICollection<etapa1> etapa1 { get; set; }
    }
}