using System;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.RTA
{
    public class Causas_inmediatas
    {
        [Key] 
        public int Causa_inmediata_id { get; set; }

        public string tipo { get; set; }
        public string Causa { get; set; }
        public string descripcion { get; set; }
        public int etapa3_id { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; }
        public etapa3 etapa3 { get; set; }
        
    }
}