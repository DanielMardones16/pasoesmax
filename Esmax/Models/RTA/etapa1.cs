using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.RTA
{
    public class etapa1
    {
        [Key]
        public int etapa1_id { get; set; }
        public int responsable1_id { get; set; }
        public string responsable1 { get; set; }
        public int holding_id { get; set; }
        public string holding_nombre { get; set; }
        public int negocio_id { get; set; }
        public string negocio_nombre { get; set; }
        public int gerencia_id { get; set; }
        public string gerencia_nombre { get; set; }
        public int area_id { get; set; }
        public string area_nombre { get; set; }
        public string identificado_como { get; set; }
        public string clasificacion { get; set; }
        public string tipo { get; set; }
        public string afeccion_anomalia { get; set; }
        public int? medio_ambiente { get; set; }
        public int? personas { get; set; }
        public int? patrimonio { get; set; }
        public int? procesos_productos { get; set; }
        
        public string clase { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? fecha_etapa1 { get; set; }
        public string hora_etapa1 { get; set; }
        public int instalacion_id { get; set; }
        public string instalacion_nombre { get; set; }
        public string lugar { get; set; }
        public int accidente { get; set; }
        public int? vehiculo_id { get; set; }
        public string vehiculo_tipo { get; set; }
        public string descripcion { get; set; }
        public string accion_inmediata { get; set; }
        public int responsable2_id { get; set; }
        public string responsable2_nombre { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; }
        
        public virtual Holding Holding { get; set; }
        public virtual Negocio Negocio { get; set; }
        public virtual Gerencia Gerencia { get; set; }
        public virtual Area Area { get; set; }
        public virtual Instalaciones Instalaciones { get; set; }
        public virtual Vehiculos Vehiculos { get; set; }
        public virtual ICollection<ArchivoRTA> ArchivoRta { get; set; }
        public virtual ICollection<RespuestaRTA> RespuestaRTA { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}