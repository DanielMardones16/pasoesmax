using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.RTA
{
    public class etapa3
    {
        [Key]
        public int etapa3_id { get; set; }
        public string tipo_metodologia { get; set; }
        public string analisis { get; set; }
        public int responsable4_id { get; set; }
        public string responsable4_nombre { get; set; }
        public string aprobacion { get; set; } = "aprobado";
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; }
        public virtual ICollection<RespuestaRTA> RespuestaRTA { get; set; }
        public virtual ICollection<ArchivoRTA> ArchivoRTA { get; set; }
        public virtual ICollection<Causas_inmediatas> Causas_inmediatas { get; set; }
        public virtual ICollection<Acciones_corr_prev> Acciones_corr_prev { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}