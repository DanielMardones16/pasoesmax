using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Helpers;
using System.Web.Mvc;

namespace Esmax.Models
{
    public class TareasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        
        [HttpGet]
        [Authorize]
        public ActionResult getTareas()
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<Tareas> tareas = db.Tareas.ToList<Tareas>();
            return Json(tareas, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        [Authorize]
        public ActionResult addTarea(Tareas tareas)
        {
            var saved = db.Tareas.Add(new Tareas
            {
                tareas_titulo = tareas.tareas_titulo,
                created_at = DateTime.Now
            });
            db.SaveChanges();
            return Json(saved, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getTareasId(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var tareas = db.OdtTareas.Where(x => x.odts_id == id).ToList();

            var t = db.Tareas.ToList();
            
            var tar = new List<Tareas>();

            foreach (var tarea in tareas)
            {
                foreach (var ta in t)
                {
                    if (ta.tareas_id == tarea.tareas_id)
                    {
                        tar.Add(new Tareas
                        {
                            tareas_id = ta.tareas_id,
                            tareas_titulo = ta.tareas_titulo,
                            created_at = ta.created_at
                        });
                    }
                }
            }

            return Json(tar, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult modificarCondiciones(Tareas tareas)
        {
            db.Entry(tareas).State = EntityState.Modified;
            db.SaveChanges();

            return Json("modified", JsonRequestBehavior.AllowGet);
        }
    }
}