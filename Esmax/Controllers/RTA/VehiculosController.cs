using System.Linq;
using System.Web.Mvc;
using Esmax.Models;

namespace Esmax.Controllers.RTA
{
    public class VehiculosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        
        [HttpGet]
        [Authorize]
        public ActionResult GetVehiculos()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var vehiculos = db.Vehiculos.ToList();
            return Json(vehiculos, JsonRequestBehavior.AllowGet);
        } 
    }
}