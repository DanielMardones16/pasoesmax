﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Esmax.Models;
using Esmax.Models.CheckList;
using Microsoft.AspNet.Identity;

namespace Esmax.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        [Authorize]
        public ActionResult Index()
        {
            return View("Index", "_Layout");
        }
        
        [Authorize]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View("About","_Layout");
        }

        [Authorize]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View("Contact","_Layout");
        }

        [HttpPost]
        [Authorize]
        public JsonResult GetResumen(string id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            
            var usuario = (from u in db.Users
                where u.Id == id
                select u).FirstOrDefault();
            
            var datos = new List<int>();
            var cerrados = 0;
            var abiertos = 0;
            var odt = 0;
            var rsms = 0;
            var pa = 0;
            var chkl = 0;
            var rta = 0;

            if (usuario.Rol == "Administrador")
            {
                cerrados = (from c in db.PlanAccion
                            where c.estado == "cerrado"
                            select c).Count();
    
                abiertos = (from c in db.PlanAccion
                            where c.estado == "habilitado"
                            select c).Count();
    
                odt = (from v in db.PlanAccion
                       where v.modulo == 1
                       select v).Count();
    
                rsms = (from v in db.PlanAccion
                        where v.modulo == 2
                        select v).Count();
    
                pa = (from v in db.PlanAccion
                      where v.modulo == 3
                      select v).Count();
    
                chkl = (from v in db.PlanAccion
                        where v.modulo == 4
                        select v).Count();
    
                rta = (from v in db.PlanAccion
                       where v.modulo == 5
                       select v).Count();
                }
            else
            {
                cerrados = (from c in db.PlanAccion
                            where c.estado == "cerrado"
                            where c.responsable_id == id
                            select c).Count();
    
                abiertos = (from c in db.PlanAccion
                            where c.estado == "habilitado"
                            where c.responsable_id == id
                            select c).Count();
    
                odt = (from v in db.PlanAccion
                       where v.modulo == 1
                       where v.responsable_id == id
                       select v).Count();
    
                rsms = (from v in db.PlanAccion
                        where v.modulo == 2
                        where v.responsable_id == id
                        select v).Count();
    
                pa = (from v in db.PlanAccion
                      where v.modulo == 3
                      where v.responsable_id == id
                      select v).Count();
    
                chkl = (from v in db.PlanAccion
                        where v.modulo == 4
                        where v.responsable_id == id
                        select v).Count();
    
                rta = (from v in db.PlanAccion
                       where v.modulo == 5
                       where v.responsable_id == id
                       select v).Count();
            }

            datos.Add(cerrados);
            datos.Add(abiertos);
            datos.Add(odt);
            datos.Add(rsms);
            datos.Add(pa);
            datos.Add(chkl);
            datos.Add(rta);
            
            return Json(datos);
        }
        [HttpPost]
        [Authorize]
        public JsonResult RsmsHolding(string id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            
            var result = new List<int>();
            
            var dist = (from d in db.RespuestaRsms
                where d.holding_nombre == "Esmax Dist."
                select d).Count();
            
            var red = (from d in db.RespuestaRsms
                where d.holding_nombre == "Esmax Red"
                select d).Count();
            
            var ind = (from d in db.RespuestaRsms
                where d.holding_nombre == "Esmax Ind."
                select d).Count();

            var total = dist + red + ind;

            var leve = (from r in db.RespuestaRsms
                where r.leve1 == 1
                select r).Count();
            
            var moderado = (from r in db.RespuestaRsms
                where r.moderado1 == 1
                select r).Count();
            
            var inaceptable = (from r in db.RespuestaRsms
                where r.inaceptable1 == 1
                select r).Count();
            
            result.Add(dist);
            result.Add(red);
            result.Add(ind);
            result.Add(total);
            result.Add(leve);
            result.Add(moderado);
            result.Add(inaceptable);
            
            return Json(result);
        }
        [HttpPost]
        [Authorize]
        public JsonResult RtaResumen(string id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var result = new List<int>();

            var reportado = (from r in db.RespuestaRta
                where r.estado == "Reportado"
                select r).Count();
            
            var investigado = (from r in db.RespuestaRta
                where r.estado == "Investigado"
                select r).Count();
            
            var aprobacion = (from r in db.RespuestaRta
                where r.estado == "En aprobación"
                select r).Count();
            
            var aprobado = (from r in db.RespuestaRta
                where r.estado == "Aprobado"
                select r).Count();
            
            var asignado = (from r in db.RespuestaRta
                where r.estado == "Costos asignados"
                select r).Count();
            
            var eficaz = (from r in db.RespuestaRta
                where r.estado == "Cerrado con tratamiento eficaz"
                select r).Count();
            
            var ineficaz = (from r in db.RespuestaRta
                where r.estado == "Cerrado con tratamiento ineficaz"
                select r).Count();

            
            var respuestasPas = (from r in db.RespuestaRta
                where r.estado == "Cerrado con tratamiento ineficaz" ||
                r.estado == "Cerrado con tratamiento eficaz"
                select r).ToList();

            var pasados = 0;
            
            foreach (var item in respuestasPas)
            {
                var et5 = (from p in db.etapa5
                    where p.etapa5_id == item.etapa5_id
                    select p.total_costos).FirstOrDefault();

                pasados = pasados + et5;

            }
            //var totalPasados = pasados.Sum();
            
            
            var respuestasFut = (from r in db.RespuestaRta
                where r.estado != "Cerrado con tratamiento ineficaz" &&
                r.estado != "Cerrado con tratamiento eficaz"
                select r).ToList();

            var futuros = 0;
            
            foreach (var item in respuestasFut)
            {
                var et5 = (from p in db.etapa5
                    where p.etapa5_id == item.etapa5_id
                    select p.total_costos).FirstOrDefault();

                futuros = futuros + et5;

            }
            //var totalFuturos = futuros.Sum();
            /*var pasados = (from r in db.RespuestaRta
                join e5 in db.etapa5
                    on r.etapa5_id equals e5.etapa5_id
                where r.estado == "Cerrado con tratamiento eficaz"
                where r.estado == "Cerrado con tratamiento ineficaz"
                select e5).ToList();*/     
            //var totalPasados = pasados.AsEnumerable().Sum(e5 => e5.total_costos);
            /*var futuros = (from r in db.RespuestaRta
                join e5 in db.etapa5
                    on r.etapa5_id equals e5.etapa5_id
                where r.estado != "Cerrado con tratamiento eficaz"
                where r.estado != "Cerrado con tratamiento ineficaz"
                select e5).ToList();

            var totalfuturos = futuros.Sum(x => x.total_costos);*/
            
            result.Add(reportado);
            result.Add(investigado);
            result.Add(aprobacion);
            result.Add(aprobado);
            result.Add(asignado);
            result.Add(eficaz);
            result.Add(ineficaz);
            result.Add(pasados);
            result.Add(futuros);
            
            return Json(result);
        }

        [HttpPost]
        [Authorize]
        public JsonResult CapResumen()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var result = new List<int>();

            var habilitados = (from t in db.Tests where t.estado.Equals("Habilitado") select t).Count();

            var deshabilitados = (from t in db.Tests where t.estado.Equals("Deshabilitado") select t).Count();

            var resultados = (from r in db.ResultadosTests select r).Count();
            
            var aprobados = (from r in db.ResultadosTests where r.aprobado == true select r).Count();

            var reprobados = (from r in db.ResultadosTests where r.aprobado == false select r).Count();

            var porcentaje = (aprobados * 100)/resultados;
            
            result.Add(habilitados);
            result.Add(deshabilitados);
            result.Add(resultados);
            result.Add(aprobados);
            result.Add(reprobados);
            result.Add(porcentaje);
            
            Console.WriteLine("HABILITADOS: " + result[0]);
            Console.WriteLine("DESHABILITADOS: " + result[1]);
            Console.WriteLine("TOTAL RESPONDIDOS: " + result[2]);
            Console.WriteLine("APROBADOS: " + result[3]);
            Console.WriteLine("REPROBADOS: " + result[4]);
            Console.WriteLine("PORCENTAJE: "+ result[5]);

            return Json(result);
        }
        
        
        
        
        
        
        [HttpPost]
        [Authorize]
        public JsonResult OdtResumen(string id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var result = new List<int>();

            var habilitadas = (from o in db.Odts
                where o.estado == "habilitado"
                select o).Count();
            
            var deshabilitadas = (from o in db.Odts
                where o.estado == "deshabilitado"
                select o).Count();

            var respDist = (from r in db.RespuestaOdts
                where r.holdingName == "Esmax Distr."
                select r).Count();
            
            var respRed = (from r in db.RespuestaOdts
                where r.holdingName == "Esmax red"
                select r).Count();
            
            var respInd = (from r in db.RespuestaOdts
                where r.holdingName == "Esmax ind"
                select r).Count();

            var total = respInd + respRed + respDist;
            
            result.Add(habilitadas);
            result.Add(deshabilitadas);
            result.Add(respDist);
            result.Add(respRed);
            result.Add(respInd);
            result.Add(total);

            return Json(result);
        }
        [HttpPost]
        [Authorize]
        public JsonResult ChklResumen(string id)
        {
            db.Configuration.ProxyCreationEnabled = false;

            var result = new List<int>();
            var distInt = 0.0;
            var redInt = 0.0;
            var indInt = 0.0;
            
            var total = (from c in db.RespuestaCheckList
                select c.respuestaCheckListId).Count();

            var aux1 = (from a in db.RespuestaCheckList
                where a.holding_nombre == "Esmax Dist."
                select a.respuestaCheckListId).Count();
            if (aux1 > 0)
            {
                var dist = (from c in db.RespuestaCheckList
                               where c.holding_nombre == "Esmax Dist."
                               select c.Porcentaje).Average();
                distInt = Math.Round(dist);
            }
            
            var aux2 = (from a in db.RespuestaCheckList
                where a.holding_nombre == "Esmax red"
                select a.respuestaCheckListId).Count();
            if (aux2 > 0)
            {
                var red = (from c in db.RespuestaCheckList
                              where c.holding_nombre == "Esmax red"
                              select (int?)c.Porcentaje).Average() ?? 0.0;
                redInt = Math.Round(red);
            }
            
            var aux3 = (from a in db.RespuestaCheckList
                where a.holding_nombre == "Esmax ind"
                select a.respuestaCheckListId).Count();
            if (aux3 > 0)
            {
                var ind = (from c in db.RespuestaCheckList
                              where c.holding_nombre == "Esmax ind"
                              select (int?)c.Porcentaje).Average() ?? 0.0;
                indInt = Math.Round(ind);
            }
            
            result.Add(total);
            result.Add((int)distInt);
            result.Add((int)redInt);
            result.Add((int)indInt);

            return Json(result);
        }
    }
}