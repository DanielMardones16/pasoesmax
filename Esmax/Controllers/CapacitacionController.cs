using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.UI.WebControls.Expressions;
using Esmax.Models;
using Esmax.Models.Capacitacion;
using Microsoft.Ajax.Utilities;

namespace Esmax.Controllers
{
    public class resultados
    {
        public string mensaje;
        public string aprobacion;
        public string estado;
        public int IdTest;
    }

    public class alternativa
    {
        public string resp;
    }

    
    
    public class CapacitacionController : Controller
    {
        
      
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET
        public ActionResult Index()
        {
            ViewBag.idDeshabilitado = 0;
            return View("SelecCapacitacion", "_Layout");
        }

        [HttpGet]
        [Authorize]
        public JsonResult validarRol(string id)
        {
            int nivel;
            int miId = int.Parse(id);
            var rol = (from au in db.Users where au.Id.Equals(id) select au.area_id).FirstOrDefault();
            var adm = (from au in db.Users where au.Id.Equals(id) select au.Rol).FirstOrDefault();
            var area = (from ar in db.Areas where ar.area_id == rol select ar.responsable_id).FirstOrDefault();

            
            if (id.Equals(area))
            {
                nivel = 2;
            }
            else
            {
                nivel = 3;
            }
            if (adm.Equals("Administrador"))
            {
                nivel = 1;
            }

            ViewBag.nivelUsuario = nivel;
            
            return Json(nivel, JsonRequestBehavior.AllowGet);

        }

        public ActionResult ResultadosMisTests()
        {
            return View("Resultados", "_Layout");
        }

        public ActionResult SeleccionarTest()
        {
            return View("SelecCapacitacion", "_Layout");
        }

        public ActionResult ResultadosGenerales()
        {
            return View("ResultadosGenerales", "_Layout");
        }

        public ActionResult CrearTest()
        {
            return View("CrearTest", "_Layout");
        }

        public ActionResult GraficasTest()
        {
            return  View("GraficosCapacitaciones", "_Layout");
        }
        
        [HttpGet]
        [Authorize]
        public JsonResult GetTests()
        {
            var lista = (from t in db.Tests where t.estado.Equals("Habilitado") select t).ToList();

            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Authorize]
        public JsonResult GetTestHabilitar()
        {
            var lista = (from t in db.Tests where t.estado.Equals("Deshabilitado") select t).ToList();

            return Json(lista, JsonRequestBehavior.AllowGet);
        }
        
        

        [HttpGet]
        [Authorize]
        public JsonResult GetResultados(string id_user)
        {
            var lista = (from r in db.ResultadosTests   where r.id_user.Equals(id_user) 
                select new {ID = r.id_resultado ,TEST= (from t in db.Tests where t.id_test == r.id_test select t.nombre), 
                 ESTADO = (r.aprobado == true ? "Aprobado":"Reprobado"), 
                 FECHA=r.fecha_test_tomado.Value.Day +" /"+ r.fecha_test_tomado.Value.Month +" /"+r.fecha_test_tomado.Value.Year
                 + "  " + r.fecha_test_tomado.Value.Hour + ":" + r.fecha_test_tomado.Value.Minute
                }).ToList();

            
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Authorize]
        public JsonResult GetResultadosGenerales()
        {
            var lista = (from r in db.ResultadosTests 
                select new {EVALUADO= (from u in db.Users where u.Id.Equals(r.id_user) select u.UserName),ID = r.id_resultado ,TEST= (from t in db.Tests where t.id_test == r.id_test select t.nombre), 
                    ESTADO = (r.aprobado == true ? "Aprobado":"Reprobado"), 
                    FECHA=r.fecha_test_tomado.Value.Day +" /"+ r.fecha_test_tomado.Value.Month +" /"+r.fecha_test_tomado.Value.Year
                          + "  " + r.fecha_test_tomado.Value.Hour + ":" + r.fecha_test_tomado.Value.Minute
                }).ToList();
            return Json(lista, JsonRequestBehavior.AllowGet);
        }


        public ActionResult getDetalleResultado(int id)
        {
            int id_test = (from t in db.ResultadosTests where t.id_resultado == id select t.id_test).FirstOrDefault();
            ViewBag.idTestDetalle = id_test;
            ViewBag.id_detalle = id;
            
            return View("DetalleResultado", "_Layout");
        }


        [HttpGet]
        [Authorize]
        public JsonResult GetInfoResultado(int id)
        {
            ResultadosTest res = (from r in db.ResultadosTests where r.id_resultado == id select r).FirstOrDefault();
            var respuestas = (from t in db.PregTests where t.id_test == res.id_test select t).ToList();
            int correctas = 0;
            string aprobado = "";
            int porcentaje = 0;

            for (int i = 0; i < respuestas.Count; i++)
            {
                if (i + 1 == 1)
                {
                    if (res.resp1 == respuestas[i].correcta)
                    {
                        correctas++;
                    }
                }
                if (i + 1 == 2)
                {
                    if (res.resp2 == respuestas[i].correcta)
                    {
                        correctas++;
                    }
                }
                if (i + 1 == 3)
                {
                    if (res.resp3 == respuestas[i].correcta)
                    {
                        correctas++;
                    }
                }
                if (i + 1 == 4)
                {
                    if (res.resp4 == respuestas[i].correcta)
                    {
                        correctas++;
                    }
                }
                if (i + 1 == 5)
                {
                    if (res.resp5 == respuestas[i].correcta)
                    {
                        correctas++;
                    }
                }
                if (i + 1 == 6)
                {
                    if (res.resp6 == respuestas[i].correcta)
                    {
                        correctas++;
                    }
                }
                if (i + 1 == 7)
                {
                    if (res.resp7 == respuestas[i].correcta)
                    {
                        correctas++;
                    }
                }
                if (i + 1 == 8)
                {
                    if (res.resp8 == respuestas[i].correcta)
                    {
                        correctas++;
                    }
                }
                if (i + 1 == 9)
                {
                    if (res.resp9 == respuestas[i].correcta)
                    {
                        correctas++;
                    }
                }
                if (i + 1 == 10)
                {
                    if (res.resp10 == respuestas[i].correcta)
                    {
                        correctas++;
                    }
                }
            }
            porcentaje = ((correctas * 100) / respuestas.Count);
            if (respuestas.Count <= 4 && porcentaje == 75)
            {
                porcentaje = 80;
            }
            
            
            resultados miRes = new resultados();

            miRes.IdTest = res.id_test;
            miRes.mensaje = "Ha respondido correctamente " + correctas + " de " + respuestas.Count +
                          " preguntas"; 
            miRes.aprobacion = "Su porcentaje de aprobación es de: " + porcentaje + "%";
            if (res.aprobado)
            {
                miRes.estado = "Aprobado";
            }
            else
            {
                miRes.estado = "Reprobado";
            }
            
            Console.WriteLine("RES: " + miRes.mensaje);
            Console.WriteLine("RES: " + miRes.aprobacion);
            Console.WriteLine("RES: " + miRes.estado);
            Console.WriteLine("RES: " + miRes.IdTest);

            
            return Json(miRes, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult getIdTest(int id_res)
        {
            int id = (from t in db.ResultadosTests where t.id_resultado == id_res select t.id_test).FirstOrDefault();
            
            return Json(id, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Authorize]
        public JsonResult GetMisAlternativas(int id_res)
        {
            ResultadosTest id = (from t in db.ResultadosTests where t.id_resultado == id_res select t).FirstOrDefault();
            int total = (from t in db.PregTests where t.id_test == id.id_test select t).Count();
            List<alternativa> alts = new List<alternativa>();
            
            
            for (int i = 0; i < total; i++)
            {
                alternativa alt = new alternativa();
                if (i + 1 == 1)
                {
                    alt.resp = id.resp1;
                }
                if (i + 1 == 2)
                {
                    alt.resp = id.resp2;
                }
                if (i + 1 == 3)
                {
                    alt.resp = id.resp3;
                }
                if (i + 1 == 4)
                {
                    alt.resp = id.resp4;
                }
                if (i + 1 == 5)
                {
                    alt.resp = id.resp5;
                }
                if (i + 1 == 6)
                {
                    alt.resp = id.resp6;
                }
                if (i + 1 == 7)
                {
                    alt.resp = id.resp7;
                }
                if (i + 1 == 8)
                {
                    alt.resp = id.resp8;
                }
                if (i + 1 == 9)
                {
                    alt.resp = id.resp9;
                }
                if (i + 1 == 10)
                {
                    alt.resp = id.resp10;
                }
                alts.Add(alt);
            }

            return Json(alts, JsonRequestBehavior.AllowGet);
            
        }
        
        [HttpGet]
        [Authorize]
        public JsonResult GetNombreTest(int id)
        {
            string nombre = (from t in db.Tests where t.id_test == id select t.nombre).FirstOrDefault();
            return Json(nombre, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        [Authorize]
        public JsonResult GetPreguntas(int id)
        {
            
            var lista = (from p in db.PregTests where p.id_test == id select p).ToList();

            return Json(lista, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        [Authorize]
        public JsonResult GetIdRespuestas(int id)
        {
            var lista = (from r in db.PregTests
                where r.id_test == id select r.id_pregunta).ToList();

            return Json(lista, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        [Authorize]
        public ActionResult responderCapacication(int id)
        {
            ViewBag.id_test = id;
            
            return View("Capacitacion", "_Layout");
        }

        [HttpGet]
        [Authorize]
        public JsonResult GetCantidad(int id_test)
        {
            var cant = (from c in db.PregTests where c.id_test == id_test select c).Count();

            ViewBag.cantidad_preguntas = cant;
            
            Console.WriteLine("VIEWBAG: "+ViewBag.cantidad_preguntas);
            
            return Json(cant, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        [Authorize]
        public JsonResult GetUnaPregunta(int id_test, int index_pregunta)
        {
            var preguntas = (from p in db.PregTests where p.id_test == id_test select p).ToArray();

            var miPregunta = preguntas[index_pregunta];

            return Json(miPregunta, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        [Authorize]
        public ActionResult verResultados()
        {
            return View("Resultados", "_Layout");
        }

        [HttpGet]
        [Authorize]
        public ActionResult volverSeleccionar()
        {
            return View("SelecCapacitacion", "_Layout");
        }

        [HttpPost]
        [Authorize]
        public JsonResult obtenerResultado(string[] respuestas, int id_test, string id_user, int total)
        {
            resultados res = new resultados();
            
            var preg = (from p in db.PregTests where p.id_test == id_test select p.correcta).ToArray();

            int cantidad = 0;
            bool aprobado;
            
            ResultadosTest misResultados = new ResultadosTest();
            
            misResultados.fecha_test_tomado = DateTime.Now;
            misResultados.id_user = id_user;
            misResultados.id_test = id_test;
            
            for (int i = 0; i < preg.Length; i++)
            {
                if (i + 1 == 1)
                {
                    misResultados.resp1 = respuestas[i];
                }
                if (i + 1 == 2)
                {
                    misResultados.resp2 = respuestas[i];
                }
                if (i + 1 == 3)
                {
                    misResultados.resp3 = respuestas[i];
                }
                if (i + 1 == 4)
                {
                    misResultados.resp4 = respuestas[i];
                }
                if (i + 1 == 5)
                {
                    misResultados.resp5 = respuestas[i];
                }
                if (i + 1 == 6)
                {
                    misResultados.resp6 = respuestas[i];
                }
                if (i + 1 == 7)
                {
                    misResultados.resp7 = respuestas[i];
                }
                if (i + 1 == 8)
                {
                    misResultados.resp8 = respuestas[i];
                }
                if (i + 1 == 9)
                {
                    misResultados.resp9 = respuestas[i];
                }
                if (i + 1 == 10)
                {
                    misResultados.resp10 = respuestas[i];
                }
                
                if (respuestas[i].Equals(preg[i]))
                {
                    cantidad++;
                }
            }
            
            int porcentaje = ((cantidad * 100) / total);
            
            

            //Guarda los resultados en la base de datos

            if (total <= 4 && porcentaje == 75)
            {
                porcentaje = 80;
            }

            if (porcentaje >= 80)
            {
                aprobado = true;
            }
            else
            {
                aprobado = false;
            }

            misResultados.aprobado = aprobado;
            
            ResultadosTest resultadosDB = db.ResultadosTests.Add(misResultados);
            db.SaveChanges();

            res.mensaje = "Ha respondido correctamente " + cantidad + " de " + total +
                          " preguntas"; 
            res.aprobacion = "Su porcentaje de aprobación es de: " + porcentaje + "%";
            if (aprobado)
            {
                res.estado = "Aprobado";
            }
            else
            {
                res.estado = "Reprobado";
            }
            
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult obtenerMensaje()
        {
            int resp = ViewBag.resp_correctas;
            int total = ViewBag.cantidad_preguntas;
            
            int porcentaje = ((resp*100)/total);

            ViewBag.porcentaje = porcentaje;
            
            
            string mensaje = "Ha respondido " + ViewBag.resp_correctas + " de " + ViewBag.cantidad_preguntas + " preguntas\n Su porcentaje de aprobación es de: " + porcentaje;
            Console.WriteLine("ViewBag.resp_correctas: " + ViewBag.resp_correctas + " ******** ViewBag.cantidad_preguntas" + ViewBag.cantidad_preguntas);
            
            
            return Json(mensaje, JsonRequestBehavior.AllowGet);
        }

        
        public ActionResult modificarCapacitacion(int id)
        {
            ViewBag.idModificado = id;
            return View("ModificarCapacitacion", "_Layout");
        }

        [HttpGet]
        [Authorize]
        public ActionResult deshabilitarCapacitacion(int id)
        {
            var Test = (from t in db.Tests where t.id_test == id select t).FirstOrDefault();

            Test.estado = "Deshabilitado";

            db.SaveChanges();
            return RedirectToAction("Index", "Capacitacion");
        }

        [HttpGet]
        [Authorize]
        public ActionResult habilitarCapacitacion(int id)
        {
            var Test = (from t in db.Tests where t.id_test == id select t).FirstOrDefault();

            Test.estado = "Habilitado";

            db.SaveChanges();
            return RedirectToAction("HabilitarTest", "Capacitacion");
        }
        

        public ActionResult HabilitarTest()
        {
            return View("HabilitarCapacitacion", "_Layout");
        }

        [HttpGet]
        [Authorize]
        public JsonResult ingresarTest(string nombre)
        {
            Tests nuevoTest = new Tests();

            nuevoTest.nombre = nombre;
            nuevoTest.estado = "Habilitado";
            db.Tests.Add(nuevoTest);
            db.SaveChanges();
            
            int id = (from t in db.Tests select t.id_test).Max();

            return Json(id, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult ingresarPreguntas(PregTests[] preguntas)
        {
            foreach (var item in preguntas)
            {
                PregTests nuevaPregunta = new PregTests();
                nuevaPregunta.id_pregunta = item.id_pregunta;
                nuevaPregunta.id_test = item.id_test;
                nuevaPregunta.descripcion = item.descripcion;
                nuevaPregunta.a = item.a;
                nuevaPregunta.b = item.b;
                nuevaPregunta.c = item.c;
                nuevaPregunta.d = item.d;
                nuevaPregunta.e = item.e;
                nuevaPregunta.correcta = item.correcta;

                db.PregTests.Add(nuevaPregunta);
                db.SaveChanges();
            }

            return Json(true,JsonRequestBehavior.AllowGet);

        }


        [HttpGet]
        [Authorize]
        public JsonResult modificarNombreTest(int id_test, string nuevoNombre)
        {
            var test = (from t in db.Tests where t.id_test == id_test select t).FirstOrDefault();
            test.nombre = nuevoNombre;

            db.SaveChanges();
            
            return Json(true,JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        [Authorize]
        public JsonResult modificarPreguntas(PregTests[] preguntas)
        {
            foreach (var item in preguntas)
            {
                PregTests modificado = (from p in db.PregTests where p.id_pregunta == item.id_pregunta select p).FirstOrDefault();
                modificado.descripcion = item.descripcion;
                modificado.a = item.a;
                modificado.b = item.b;
                modificado.c = item.c;
                modificado.d = item.d;
                modificado.e = item.e;
                modificado.correcta = item.correcta;

                db.SaveChanges();
            }
            
            return Json(true,JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [Authorize]
        public JsonResult modificarNuevasPreguntas(PregTests[] preguntas)
        {
            foreach (var item in preguntas)
            {
                PregTests modificado = (from p in db.PregTests where p.id_pregunta == item.id_pregunta select p).FirstOrDefault();

                if (modificado != null)
                {
                    modificado.descripcion = item.descripcion;
                    modificado.a = item.a;
                    modificado.b = item.b;
                    modificado.c = item.c;
                    modificado.d = item.d;
                    modificado.e = item.e;
                    modificado.correcta = item.correcta;

                    db.SaveChanges();
                }
                else
                {
                    PregTests nuevaPregunta = new PregTests();

                    nuevaPregunta.id_pregunta = item.id_pregunta;
                    nuevaPregunta.id_test = item.id_test;
                    nuevaPregunta.descripcion = item.descripcion;
                    nuevaPregunta.a = item.a;
                    nuevaPregunta.b = item.b;
                    nuevaPregunta.c = item.c;
                    nuevaPregunta.d = item.d;
                    nuevaPregunta.e = item.e;
                    nuevaPregunta.correcta = item.correcta;

                    db.PregTests.Add(nuevaPregunta);
                    db.SaveChanges();

                }
                
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [Authorize]
        public JsonResult getResultadosFecha(string desde, string hasta)
        {
            DateTime desdeFecha = DateTime.Parse(desde);
            DateTime hastaFecha = DateTime.Parse(hasta);
            var resul = (from r in db.ResultadosTests where r.fecha_test_tomado >= desdeFecha && r.fecha_test_tomado <= hastaFecha group r by r.id_test into resGroup
                select new
                {
                    test = (from t in db.Tests where t.id_test == resGroup.Key select t.nombre.Substring(0,30) + "..."),
                    total = resGroup.Count(),
                })
                ;

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult getUsuariosResponden(string desde, string hasta)
        {
            DateTime desdeFecha = DateTime.Parse(desde);
            DateTime hastaFecha = DateTime.Parse(hasta);
            var resul = (from r in db.ResultadosTests  where r.fecha_test_tomado >= desdeFecha && r.fecha_test_tomado <= hastaFecha group r by r.id_user into resGroup
                select new
                {
                    nombre = (from u in db.Users where u.Id == resGroup.Key select u.UserName),
                    total = resGroup.Count(),
                }
            );
            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult getAprobados(string desde, string hasta)
        {
            DateTime desdeFecha = DateTime.Parse(desde);
            DateTime hastaFecha = DateTime.Parse(hasta);
            var resul = (from r in db.ResultadosTests
                where r.fecha_test_tomado >= desdeFecha && r.fecha_test_tomado <= hastaFecha group r by (from tot in db.ResultadosTests select tot.id_resultado).Count() into resGroup
                select new
                {
                    total = (from t in db.ResultadosTests select t.id_resultado).Count(),
                    aprobados = (from a in db.ResultadosTests where a.aprobado == true select a).Count(),
                }
                );

            return Json(resul, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [Authorize]
        public JsonResult getReprobados(string desde, string hasta)
        {
            DateTime desdeFecha = DateTime.Parse(desde);
            DateTime hastaFecha = DateTime.Parse(hasta);
            var resul = (from r in db.ResultadosTests
                    where r.fecha_test_tomado >= desdeFecha && r.fecha_test_tomado <= hastaFecha group r by (from tot in db.ResultadosTests select tot.id_resultado).Count() into resGroup
                    select new
                    {
                        total = (from t in db.ResultadosTests select t.id_resultado).Count(),
                        reprobados = (from a in db.ResultadosTests where a.aprobado == false select a).Count(),
                    }
                );

            return Json(resul, JsonRequestBehavior.AllowGet); 
        }


        [HttpPost]
        [Authorize]
        public JsonResult getPorcentajeFecha(string desde, string hasta)
        {
            DateTime desdeFecha = DateTime.Parse(desde);
            DateTime hastaFecha = DateTime.Parse(hasta);
            int total = (from t in db.ResultadosTests  where t.fecha_test_tomado >= desdeFecha && t.fecha_test_tomado <= hastaFecha select t.id_resultado).Count();
            int reprobados = (from a in db.ResultadosTests
                where a.fecha_test_tomado >= desdeFecha && a.fecha_test_tomado <= hastaFecha && a.aprobado == true
                select a).Count();
            int resul = (reprobados * 100) / total;

            return Json(resul, JsonRequestBehavior.AllowGet); 
        }


    }
}