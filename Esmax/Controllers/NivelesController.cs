using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Esmax.Models
{
    public class NivelesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        
        [HttpGet]
        [Authorize]
        public ActionResult getNiveles()
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<Niveles> niveles = db.Niveles.ToList<Niveles>();
            return Json(niveles, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public ActionResult addNivel(Niveles nivel)
        {
            var saved = db.Niveles.Add(new Niveles
            {
                niveles_titulo = nivel.niveles_titulo,
                niveles_codigo = nivel.niveles_codigo,
                created_at = DateTime.Now
            });
            db.SaveChanges();
            return Json(saved, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult getOdtNiveles(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var odt = db.Odts.GroupJoin(db.NivelesOdts, od => od.odts_id,
                no => no.odts_id,  (od, no) => new {od, no}).FirstOrDefault(x => x.od.odts_id == id);
            return Json(odt, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult getObsId(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var niveles = db.NivelesOdts.Where(x => x.odts_id == id).ToList();

            var n = db.Niveles.ToList();
            
            var niv = new List<Niveles>();

            foreach (var nivel in niveles)
            {
                foreach (var ni in n)
                {
                    if (ni.niveles_id == nivel.niveles_id)
                    {
                        niv.Add(new Niveles
                        {
                            niveles_id = ni.niveles_id,
                            niveles_codigo = ni.niveles_codigo,
                            niveles_titulo = ni.niveles_titulo,
                            created_at = ni.created_at
                        });
                    }
                }
            }

            return Json(niv, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public JsonResult modificarNivel(Niveles nivel)
        {
            db.Entry(nivel).State = EntityState.Modified;
            db.SaveChanges();

            return Json("modified", JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        [Authorize]
        public JsonResult findNivelesOdt(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var NivelesOdt = db.NivelesOdts.Where(x => x.NivelesOdts_id == id).FirstOrDefault();
            return Json(NivelesOdt);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult findNiveles(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var Nivel = db.Niveles.Where(x => x.niveles_id == id).FirstOrDefault();
            return Json(Nivel);
        }
        
        
        
        
    }
}