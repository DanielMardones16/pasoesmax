using System.Linq;
using System.Web.Mvc;
using Esmax.Models;

namespace Esmax.Controllers
{
    public class AnalisisController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        
        [HttpPost]
        [Authorize]
        public JsonResult getAnalisisR(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var rsp = db.Analisis.Where(r => r.respuesta_odt_id == id).FirstOrDefault();

            return Json(rsp, JsonRequestBehavior.AllowGet);
        }
    }
}